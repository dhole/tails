# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Tails\n"
"POT-Creation-Date: 2015-01-25 20:07+0100\n"
"PO-Revision-Date: 2015-09-23 20:10+0100\n"
"Last-Translator: spriver <spriver@autistici.org>\n"
"Language-Team: DE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"
"Language: de\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Manual installation using Windows\"]]\n"
msgstr "[[!meta title=\"Händische Installation unter Windows\"]]\n"

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"doc/first_steps/manual_usb_installation.intro\" raw=\"yes\"]]\n"
msgstr "[[!inline pages=\"doc/first_steps/manual_usb_installation.intro\" raw=\"yes\"]]\n"

#. type: Plain text
msgid ""
"This technique uses the Universal USB Installer, for more info or more help "
"visit [http://www.pendrivelinux.com/](http://www.pendrivelinux.com/universal-"
"usb-installer-easy-as-1-2-3/)."
msgstr ""
"Bei dieser Vorgehensweise wird der Universal USB Installer benutzt, für mehr "
"Informationen oder Hilfe besuchen Sie bitte [http://www.pendrivelinux.com/]"
"(http://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/)."

#. type: Title ###
#, no-wrap
msgid "Insert a USB stick with at least 2GB of free space"
msgstr "Schließen Sie einen USB-Stick mit einer Größe von mindestens 2GB an"

#. type: Title ###
#, no-wrap
msgid "[Download the Universal USB Installer](http://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/)"
msgstr "[Laden Sie den Universal USB Installer herunter](http://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/)"

#. type: Plain text
#, no-wrap
msgid ""
"<div class=\"note\">\n"
"You will need version 1.9.5.4 or later.\n"
"</div>\n"
msgstr ""
"<div class=\"note\">\n"
"Sie werden Version 1.9.5.4 oder neuer benötigen.\n"
"</div>\n"

#. type: Title ###
#, no-wrap
msgid "Click 'Run' when prompted<br/>"
msgstr "Wählen Sie 'Ausführen' wenn Sie dazu aufgefordert werden<br/>"

#. type: Plain text
#, no-wrap
msgid "[[!img windows/01-run.jpg link=no alt=\"Do you want to run or save this file?\"]]\n"
msgstr "[[!img windows/01-run.jpg link=no alt=\"Do you want to run or save this file?\"]]\n"

#. type: Title ###
#, no-wrap
msgid "If the security dialog appears, confirm by clicking 'Run'"
msgstr "Wählen Sie zum Bestätigen 'Ausführen' wenn der Sicherheitsdialog erscheint"

#. type: Plain text
#, no-wrap
msgid "[[!img windows/02-run.jpg link=no alt=\"The publisher could not be verified. Are you sure you want to run this software?\"]]\n"
msgstr "[[!img windows/02-run.jpg link=no alt=\"The publisher could not be verified. Are you sure you want to run this software?\"]]\n"

#. type: Title ###
#, no-wrap
msgid "Read the licence agreement and choose 'I Agree' to continue"
msgstr "Lesen Sie die Lizenzvereinbarung und wählen Sie 'Ich stimme zu', um fortzufahren"

#. type: Plain text
#, no-wrap
msgid "[[!img windows/03-license-agreement.png link=no alt=\"License Agreement\"]]\n"
msgstr "[[!img windows/03-license-agreement.png link=no alt=\"License Agreement\"]]\n"

#. type: Title ###
#, no-wrap
msgid "Select Tails from the dropdown list"
msgstr "Wählen Sie Tails in der Auswahlliste aus"

#. type: Plain text
#, no-wrap
msgid "[[!img windows/04-select-tails.png link=no alt=\"Step 1: Select a Linux Distribution from the dropdown to put on your USB\"]]\n"
msgstr "[[!img windows/04-select-tails.png link=no alt=\"Step 1: Select a Linux Distribution from the dropdown to put on your USB\"]]\n"

#. type: Title ###
#, no-wrap
msgid "Click 'Browse' and open the downloaded ISO file"
msgstr "Wählen Sie 'Durchsuchen' und öffnen Sie die ISO-Datei"

#. type: Plain text
#, no-wrap
msgid "[[!img windows/05-browse-iso.png link=no alt=\"Step 2: Select your tails*.iso\"]]\n"
msgstr "[[!img windows/05-browse-iso.png link=no alt=\"Step 2: Select your tails*.iso\"]]\n"

#. type: Title ###
#, no-wrap
msgid "Choose the USB stick"
msgstr "Wählen Sie den USB-Stick aus"

#. type: Plain text
#, no-wrap
msgid "[[!img windows/06-choose-drive.png link=no alt=\"Step 3: Select your USB Flash Drive Letter Only\"]]\n"
msgstr "[[!img windows/06-choose-drive.png link=no alt=\"Step 3: Select your USB Flash Drive Letter Only\"]]\n"

#. type: Title ###
#, no-wrap
msgid "Click 'Create'"
msgstr "Wählen Sie 'Erstellen'"

#. type: Plain text
#, no-wrap
msgid "[[!img windows/07-create.png link=no alt=\"Create\"]]\n"
msgstr "[[!img windows/07-create.png link=no alt=\"Create\"]]\n"

#. type: Plain text
msgid "Then safely remove the USB stick from the computer."
msgstr "Werfen Sie anschließend den USB-Stick sicher vom Computer aus."

#. type: Plain text
#, no-wrap
msgid "<div class=\"next\">\n"
msgstr "<div class=\"next\">\n"

#. type: Plain text
#, no-wrap
msgid ""
"<p>After the installation completes, you can [[start Tails|start_tails]]\n"
"from this new USB stick.</p>\n"
msgstr ""
"<p>Nachdem die Installation abgeschlossen wurde\n"
"können Sie von diesem neuen USB-Stick [[Tails starten|start_tails]].</p>\n"

#. type: Plain text
#, no-wrap
msgid "</div>\n"
msgstr "</div>\n"
